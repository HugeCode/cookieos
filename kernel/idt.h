static inline
void lidt( void * base, unsigned int size )
{
    unsigned int i[2];
 
    i[0] = size << 16;
    i[1] = (unsigned int)base;
    asm( "lidt (%0)"
         : : "p"(((char *) i)+2) );
}
