#ifndef VIDEO_H
#define VIDEO_H

enum video_type
{
    VIDEO_TYPE_NONE = 0x00,
    VIDEO_TYPE_COLOUR = 0x20,
    VIDEO_TYPE_MONOCHROME = 0x30,
};
 
inline uint16_t detect_bios_area_hardware(void)
{
    const uint16_t* bda_detected_hardware_ptr = (const uint16_t*) 0x410;
    return *bda_detected_hardware_ptr;
}

inline unsigned char get_bios_area_video_typec(void)
{
    return (detect_bios_area_hardware() & 0x30);
}

#endif
