This shall be an epic place to where adventure leads you...

How to get around
-----------------------------------
* `/` - contains the makefile, linker script and readme
* `/build/` (contents ignored by git) - contains built files and final .iso image of operating system
* `/kernel/` - contains kernel source code
* `isodir` - contains final .iso image

How to run
-----------------------------------
1. make sure you have a cross compiler placed in `~/opt/cross/` directory and grub programs (with their dependencies) installed
2. enter project`s root
3. `make` the source code in `build` directory
4. run `mk_iso.sh` in the root directory
5. attach the resulting `/build/cookieos.iso` and run your virtual machine

Things to do
-----------------------------------
1. Give the user a cookie(any 3D printer driver I can use?)
2. Code a better OS worth people actually seeing(hey, don't complain, its not easy)
3. Waste people's time by making a totally random point(check)
4. Currently, it isn't possible to have two source files with the same 
name anywhere in the kernel's code structure, because of our amazing `makefile`. 
That makefile needs to be rebuilt.
